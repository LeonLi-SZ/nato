void splashScreenA2M() {
  String s;

  if (subSM == 0) {
    fill(#1DB236);
    textSize(40);
    text("NATO Phonetic Alphabet Tester. V3.1", 40, 80);
    text("Chuyi Li. All Rights Reserved. Dec 2024.", 40, 130);
    yPos = 250;
  } else {
    fill(#98982B);
    textSize(30);
    s = natoChar[subSM - 1];
    text(s, 50, yPos);
    text(natoStr[subSM -1], 200, yPos);
    yPos += 45;
    delay(120);
  }

  subSM++;
  if (subSM >= 14) {
    mainSM = 1; // splashScreenN2Z
    subSM = 0;
    yPos = 250;
  }
}

// ============================ //

void splashScreenN2Z() {
  String s;

  s = natoChar[subSM + 13 ];
  text(s, 850, yPos);
  text(natoStr[subSM + 13], 1000, yPos);
  yPos += 45;
  delay(120);

  subSM++;
  if (subSM >= 13) {
    // wait touch screen to clear spalash screen
    mainSM = 2; // test starts
    subSM = 0;

    image(imgSeal, 1200, 500); // show Li Chuyi's seal
    image(imgPetbird, 400, 500); // show Li Chuyi's 2 pet birds

    noCursor();
    showNatoCursro();
  }
}
