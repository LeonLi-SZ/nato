// NATO phonetic alphabet tester. V3.0
// Chuyi Li. All Rights Reserved
// Feb 2024
// Dec 2024

PFont myFont;
PImage imgSeal, imgPetbird, imgNaotCursor, imgHelp;

int rndNo;
String testChs = "";
String []testStr;

int mainSM; // main state machine
int subSM; // sub state machine (for splash screen to show NATO char animation)

int yPos;

final color bgColor = #12748B;

// ============================ //

public void setup() {
  size(1600, 900, JAVA2D);
  smooth();

  background(bgColor);

  myFont = loadFont("Rockwell-BoldItalic-128.vlw");
  textFont(myFont);

  testStr = new String[5];

  imgSeal = loadImage("LiChuYi_Seal.png");
  imgPetbird = loadImage("nuomituan_zhimagao.png");
  imgNaotCursor = loadImage("natoCursor.png");
  imgHelp = loadImage("help.png");

  mainSM = 0; // spalash_screen_A2M
  subSM = 0;

  println("press 'h' to show NATO table");
}

// ============================ //

public void draw() {
  switch(mainSM) {
    // ===== splash screen  ===== //
  case 0:
    {
      splashScreenA2M();
      break;
    }
  case 1:
    {
      splashScreenN2Z();
      break;
    }

  // wait mouse event exit splash screen, starts test
  case 2:
    {
      if (toNext()) {
        background(bgColor);
        mainSM = 3;
      }
      break;
    }
    
    // ===== test starts ===== //
  case 3: // show question
    {
      showNatoCursro();

      textSize(120);
      testChs = "";

      for (int i = 0; i < 5; i++) {
        rndNo = (int)random(0, 26); // [0, 26)
        testChs += natoChar[rndNo];
        testChs += "  "; // 2 SPACEs
        testStr[i] = natoStr[rndNo];
      }

      thread("threadDlyShowAns");
      mainSM = 4; // wait some seconds to show answer
      break;
    }
  case 4: // wait tmo
    {
      background(bgColor);
      text(testChs, 430, 250);
      showNatoCursro();
      break;
    }
  case 5: // show answer
    {
      background(bgColor);
      textSize(120);
      text(testChs, 430, 250);

      textSize(60);
      yPos = 450;
      text(testStr[0], 300, yPos);
      yPos += 70;
      text(testStr[1], 300, yPos);
      yPos += 70;
      text(testStr[2], 300, yPos);
      yPos += 70;
      text(testStr[3], 300, yPos);
      yPos += 70;
      text(testStr[4], 300, yPos);

      showNatoCursro();

      if (toNext()) {
        mainSM = 3;
      }

      break;
    }

    // ===== help screen ===== //
  case 6:
    {
      background(bgColor);
      textSize(120);
      text(testChs, 430, 250);
      image(imgHelp, 61, 300);
      image(imgSeal, 1200, 500); // show Li Chuyi's seal
      image(imgPetbird, 400, 500);

      if (toNext()) {
        mainSM = 3;
      }
      break;
    }
    
    default: {
      mainSM =  3; // should not happen
    }
  }
}

// ============================ //

void showNatoCursro() {
  image(imgNaotCursor, mouseX - 60, mouseY - 40);
}

// ============================ //

void threadDlyShowAns() {
  delay(6800); // delay between quesiont and answer
  mainSM = 5;
}

// ============================ //

void keyPressed() {
  if (key == 'h') {
    mainSM = 6;
  }
}
