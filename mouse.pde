boolean isToNext = false;

// ============================ //

boolean toNext() {
  if (isToNext) {
    isToNext = false;
    return true;
  }
  return false;
}

// ============================ //

void mousePressed() {
    isToNext = true;
    thread("threadMouseDebounce");
  // println(mouseX + " " + mouseY);
}

// ============================ //

void mouseMoved() {
  if (!isToNext) {
    isToNext = true;
    thread("threadMouseDebounce");
  }
}

// ============================ //

void threadMouseDebounce() {
  delay(800);
  isToNext = false;
}
